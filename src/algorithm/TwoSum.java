/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithm;

import java.util.Arrays;

/**
 *
 * @author Khandaa
 */
public class TwoSum {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Arrays.stream(twoSum(new int[]{3, 2, 4}, 6)).forEach(System.out::println);
    }

    static public int[] twoSum(int[] nums, int target) {
        for (int k = 0; k < nums.length; k++) {
            for (int j = k; j < nums.length - 1; j++) {
                System.out.println(String.format("%s, %s, %s, %s", k, j, nums[k], nums[j]));
                j++;
                if (nums[k] + nums[j] == target) {
                    return new int[]{k, j};
                }
            }
        }
        return null;
    }

    static public int[] twoSumSort(int[] nums, int target) {
        int[] numsSorted = Arrays.copyOf(nums, nums.length);
        Arrays.sort(numsSorted);
        int i = 0;
        int j = numsSorted.length - 1;
        int a = 0, b = 0;
        while (i < numsSorted.length && j >= 0) {
            if (numsSorted[i] + numsSorted[j] == target) {
                a = numsSorted[i];
                b = numsSorted[j];
                break;
            } else if (numsSorted[i] + numsSorted[j] < target) {
                i++;
            } else {
                j--;
            }
        }
        int[] indices = new int[2];
        for (int k = 0; k < nums.length; k++) {
            if (nums[k] == a) {
                indices[0] = k;
            } else if (nums[k] == b) {
                indices[1] = k;
            }
        }
        return indices;
    }

}
